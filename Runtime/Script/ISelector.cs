﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISelector 
{

    GameObject[] SelectedObjectsWithParams();
    GameObject[] SelectedObjectWithoutParams();
    GameObject[] SelectedWithTags(string[] tags);
    GameObject[] SelectedWithLayer(LayerMask mask);
    GameObject[] SelectedWithTagsAndLayers(string[] tags, LayerMask mask);
    GameObject[] SelectedWithTagsOrLayers(string[] tags, LayerMask mask);
  

    T[] SelectedObjectsWithParams<T>() where T : Component;
    T[] SelectedWithLayer<T>(LayerMask mask) where T : Component;
    T[] SelectedWithTagsAndLayers<T>(string[] tags, LayerMask mask) where T : Component;
    T[] SelectedWithTagsOrLayers<T>(string[] tags, LayerMask mask) where T : Component;
    
}
