﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegularlyCheckSelector : MonoBehaviour
{
    [Header("Selectior Info")]
    public SelectorAbstract m_selector;
    public float m_timeBetweenCheck=0.1f;

    
    [Header("Result")]
    public GameObject[] m_selectedObjects;
    public IEnumerator Start() {

        while (true) {
            if (m_timeBetweenCheck <= 0f)
                yield return new WaitForEndOfFrame();
            else
                yield return new WaitForSeconds(m_timeBetweenCheck);

            m_selectedObjects = m_selector.SelectedObjectsWithParams();
     
        }
    }

    private void Reset()
    {
        m_selector = GetComponent<SelectorAbstract>();

    }
}
