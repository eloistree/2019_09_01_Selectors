﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class SelectorAbstract : MonoBehaviour, ISelector
{

    public abstract GameObject[] SelectedObjectsWithParams();
    public abstract GameObject[] SelectedObjectWithoutParams();
    public abstract GameObject[] SelectedWithTags(string[] tags);
    public abstract GameObject[] SelectedWithLayer(LayerMask mask);
    public abstract GameObject[] SelectedWithTagsAndLayers(string[] tags, LayerMask mask);
    public abstract GameObject[] SelectedWithTagsOrLayers(string[] tags, LayerMask mask);


    public T[] SelectedObjectsWithParams<T>() where T : Component
    {
        return FilterByScript<T>(SelectedObjectsWithParams());
    }

    public T[] SelectedWithLayer<T>(LayerMask mask) where T : Component
    {

        return FilterByScript<T>(SelectedWithLayer(mask));
    }
    public T[] SelectedWithTagsAndLayers<T>(string[] tags, LayerMask mask) where T : Component
    {

        return FilterByScript<T>(SelectedWithTagsAndLayers(tags, mask));
    }
    public  T[] SelectedWithTagsOrLayers<T>(string[] tags, LayerMask mask) where T : Component
    {
        return FilterByScript<T>(SelectedWithTagsOrLayers(tags, mask));
    }

    public static T[] FilterByScript<T>(GameObject[] selection) where T : Component
    {
        T element = null;
        List<T> elements = new List<T>();
        for (int i = 0; i < selection.Length; i++)
        {
            element = selection[i].GetComponent<T>();
        }

        return selection.Select(a => a.GetComponent<T>()).Where(k => k != null).ToArray();
    }
   
    public static GameObject[] FilterByLayer(LayerMask maskLayer, GameObject[] selection)
    {

        return selection.Where(a => IsInLayerMask(a.layer,maskLayer)).ToArray();
    }
    public static GameObject[] FilterByTags(string [] tags, GameObject[] selection)
    {

        return selection.Where(k => tags.Contains(k.gameObject.tag)).ToArray();
    }
    public static bool IsInLayerMask(int layer, LayerMask layermask)
    {
        return layermask == (layermask | (1 << layer));
    }
}
