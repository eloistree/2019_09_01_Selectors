﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SphericalSelector : SelectorAbstract
{

    [Header("Params")]
    public float m_radius=1;
    [SerializeField] Transform m_gizmo;
    [Space]
    [Header("Tags & Layer")]
    public LayerMask m_layerMask=-1;
    public TagLayerCondition m_layerCondition= TagLayerCondition.Or;
    public string [] m_tags;
    public enum TagLayerCondition { And, Or, All, JustTag, JustLayer}

    private void Awake()
    {
        SetScaleRadius(m_radius);
    }
    
    public override GameObject[] SelectedObjectsWithParams()
    {
        if (m_layerCondition == TagLayerCondition.And)
            return SelectedWithTagsAndLayers(m_tags, m_layerMask);
        if (m_layerCondition == TagLayerCondition.Or)
            return SelectedWithTagsOrLayers(m_tags, m_layerMask);
        if (m_layerCondition == TagLayerCondition.JustLayer)
            return SelectedWithLayer( m_layerMask);
        if (m_layerCondition == TagLayerCondition.JustTag )
            return SelectedWithTags(m_tags);

        return  SelectedWithLayer(this.allLayerMask);
    }
    public override GameObject[] SelectedWithTags(string [] tags)
    {



        GameObject[]  m_lastResult = SelectedWithLayer(this.allLayerMask);
        m_lastResult = RecoverThoseWithAtLeastOnOfThoseTags(tags, m_lastResult);
        return m_lastResult;
    }
    public override GameObject[] SelectedObjectWithoutParams()
    {
        return SelectedWithLayer(this.allLayerMask);

    }

    public override GameObject[] SelectedWithLayer(LayerMask mask)
    {

        Collider[]  m_selectedColliders = Physics.OverlapSphere(m_gizmo.transform.position, GetGizmoMaxLenght() * 0.5f, mask);
        return  m_selectedColliders.Select(k => k.gameObject).ToArray();
       
    }

    public override GameObject[] SelectedWithTagsAndLayers(string[] tags, LayerMask mask)
    {
        GameObject[] selected = SelectedWithLayer(mask);
        selected = RecoverThoseWithAtLeastOnOfThoseTags(tags, selected);

        return selected;

    }

    LayerMask allLayerMask = -1;
    public override GameObject[] SelectedWithTagsOrLayers(string[] tags, LayerMask mask)
    {
        GameObject[] selectedByTag = SelectedWithLayer(allLayerMask);
        selectedByTag = RecoverThoseWithAtLeastOnOfThoseTags(tags, selectedByTag);

        GameObject [] selectedByMask = SelectedWithLayer(mask);

        GameObject [] result = selectedByTag.Union(selectedByMask).ToArray() ;
        
        return result;

    }
    

    private GameObject[] RecoverThoseWithAtLeastOnOfThoseTags(string[] tags, GameObject [] objects)
    {
       return objects.Where(k => tags.Contains(k.gameObject.tag)).ToArray();
    }


    private float GetGizmoMaxLenght()
    {
        float lenght = Mathf.Abs(m_gizmo.transform.localScale.y);
        if (lenght > Mathf.Abs(m_gizmo.transform.localScale.x))
            lenght = Mathf.Abs(m_gizmo.transform.localScale.x);
        if (lenght > Mathf.Abs(m_gizmo.transform.localScale.z))
            lenght = Mathf.Abs(m_gizmo.transform.localScale.z);
        return lenght;
     }

    private void Reset()
    {
        m_gizmo = transform;
    }
    private void OnValidate()
    {
        SetScaleRadius(m_radius);
    }

    private void SetScaleRadius(float radius)
    {
        m_radius = radius;
        m_gizmo.transform.localScale = Vector3.one * radius;
    }

  
}
