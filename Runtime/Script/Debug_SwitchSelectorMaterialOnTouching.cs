﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debug_SwitchSelectorMaterialOnTouching : MonoBehaviour
{
    public SelectorAbstract m_selector;
    public Renderer m_renderer;
    public Material m_touching;
    public Material m_notTouching;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    
    void Update()
    {
        m_renderer.material = m_selector.SelectedObjectsWithParams().Length>0 ? m_touching : m_notTouching;
       
    }

    private void Reset()
    {
        m_selector
            = GetComponent<SelectorAbstract>();
    }

}
